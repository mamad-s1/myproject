import React, { useEffect, useReducer } from "react";
import { Box, colors, Grid, makeStyles } from "@material-ui/core";
import Navbar from "./pages/navbar/Navbar";
import Signup from "./pages/signup/SignForm";
import InsuranceType from "./pages/InsuranceType/InsuranceType";
import ThirdPartyInsurance from "./pages/thirdPartyInsurance/ThirdPartyInsurance";
import CarImage from "./assets/image/car-green.svg";
import PreviousInsuranceCompany from "./pages/previousInsuranceCompany/PreviousInsuranceCompany";
import OffPercent from "./pages/offPercent/OffPercent";
import { getCarsType } from "./services/Apis";

const useStyles = makeStyles((theme) => ({
  root: {
    fontFamily: "Vazir",
    margin: "0",
    height: "99vh",
    position: "relative",
  },
  formBox: {
    padding: "4rem 10%",
    paddingTop: "80px",
  },
  sideBox: {
    backgroundColor: "#fffbeb",
    paddingTop: "80px",
  },
  subject: {
    width: "70%",
    margin: "2rem auto",
    textAlign: "right",
  },
  imgBox: {
    width: "40%",
    position: "absolute",
    bottom: "50px",
    right: "150px",
  },
}));

function reducer(state, action) {
  switch (action.type) {
    case "NEXT_STEPP":
      return { ...state, activeStep: state.activeStep + 1 };
    case "BACK_STEPP":
      return { ...state, activeStep: state.activeStep - 1 };
    case "SET_USER_PROFILE":
      return {
        ...state,
        userProfile: action.payload,
      };
    case "SET_USER_CAR_TYPE":
      return {
        ...state,
        userCarType: action.payload,
      };
    case "PIC":
      return {
        ...state,
        PIC: action.payload,
      };
    case "off":
      return {
        ...state,
        off: action.payload,
      };
    default:
      break;
  }
  return state;
}

const initialState = {
  activeStep: 0,
  userProfile: {},
  userCarType: {},
  PIC: null,
  off: {},
};

export default function App() {
  const classes = useStyles();
  const [state, dispatch] = useReducer(reducer, initialState);
  const { activeStep, userProfile, userCarType } = state;

  console.log("state: ", state);
  const fieldHandler = (data) => {
    dispatch({
      type: data.type,
      payload: data.payload,
    });
  };

  const getStep = (key) => {
    switch (key) {
      case 0:
        return <Signup setField={fieldHandler} />;
      case 1:
        return <InsuranceType setField={fieldHandler} />;
      case 2:
        return <ThirdPartyInsurance setField={fieldHandler} />;
      case 3:
        return <PreviousInsuranceCompany setField={fieldHandler} />;
      case 4:
        return <OffPercent setField={fieldHandler} />;
      default:
        return null;
    }
  };

  const getCars = async () => {
    const res = await getCarsType();
    console.log("res: ", res);
  };

  useEffect(() => {
    getCars();
  }, []);

  return (
    <>
      <Navbar fName={userProfile.firstName} lName={userProfile.lastName} />
      <Grid container className={classes.root}>
        <Grid item md={8} className={classes.formBox}>
          {getStep(activeStep)}
        </Grid>
        <Grid item md={4} className={classes.sideBox}>
          ‌
        </Grid>
        <Box className={classes.imgBox}>
          <img src={CarImage} alt="CarImage" />
        </Box>
      </Grid>
    </>
  );
}
