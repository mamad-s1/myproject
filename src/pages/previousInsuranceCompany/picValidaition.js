import * as Yup from "yup";

const schema = Yup.object().shape({
  PIC: Yup.number().required("لطفا شرکت بیمه‌گر قبلی خود را وارد کنید"),
});

export default schema;
