import React from "react";
import { Grid, makeStyles, Typography } from "@material-ui/core";
import insuranceImg from "../../assets/image/insurance.svg";
import CustomIconButton from "../../components/customIconButton/CustomIconButton";

const useStyles = makeStyles((theme) => ({
  title: {
    fontFamily: "Vazir-Bold",
    marginBottom: "2rem",
    fontSize: "1.5rem",
  },
}));

export default function InsuranceType({ setField }) {
  const classes = useStyles();
  return (
    <>
      <Typography className={classes.title} variant="h1">
        انتخاب بیمه
      </Typography>
      <Grid container direction="row" spacing={2}>
        <Grid item>
          <CustomIconButton
            onClick={() => setField({ type: "NEXT_STEPP", payload: null })}
            icon={insuranceImg}
            lable="شخص ثالث"
            size="50%"
          />
        </Grid>
        <Grid item>
          <CustomIconButton
            disabled
            icon={insuranceImg}
            lable="بدنه"
            size="50%"
          />
        </Grid>
      </Grid>
    </>
  );
}
