import * as Yup from "yup";

const schema = Yup.object().shape({
  carType: Yup.number().required("لطفا نوع خودرو را وارد کنید"),
  carModel: Yup.number().required("لطفا مدل خودرو را وارد کنید"),
});

export default schema;
