import React from "react";
import { Box, Grid, makeStyles, Typography } from "@material-ui/core";
import { useForm, FormProvider } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import CustomButton from "../../components/customButton/CustomButton";
import CustomSelect from "../../components/customSelect/CustomSelect";
import schema from "./carTypeValidation";

const useStyles = makeStyles((theme) => ({
  title: {
    fontFamily: "Vazir-Bold",
    marginBottom: "1rem",
    fontSize: "1.5rem ",
  },
  subject: {
    fontSize: ".8rem",
    fontFamily: "Vazir-Bold",
  },
  btn: {
    backgroundColor: "#fff",
    color: [theme.palette.secondary.main],
    border: "1px solid",
    borderColor: [theme.palette.secondary.main],
  },
}));

export default function InsuranceType({ setField }) {
  const classes = useStyles();
  const methods = useForm({
    resolver: yupResolver(schema),
  });
  const { handleSubmit } = methods;
  const onSubmitSignup = (data) => {
    setField({ type: "SET_USER_CAR_TYPE", payload: data });
    setField({ type: "NEXT_STEPP", payload: null });
  };

  const data = [
    { value: 1, label: "yes" },
    { value: 2, label: "no" },
  ];

  return (
    <>
      <Typography className={classes.title} variant="h1">
        بیمه شخص ثالث
      </Typography>
      <FormProvider {...methods}>
        <form onSubmit={handleSubmit(onSubmitSignup)}>
          <Grid container direction="column" spacing={2}>
            <Grid item>
              <Typography className={classes.subject} variant="caption">
                نوع و مدل خودروی خود را انتخاب کنید.
              </Typography>
            </Grid>
            <Grid container item spacing={2}>
              <Grid item xs={12} md={6}>
                <CustomSelect
                  options={data}
                  placeholder="نوع خودرو"
                  name="carType"
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <CustomSelect
                  options={data}
                  placeholder="مدل خودرو"
                  name="carModel"
                />
              </Grid>
            </Grid>
            <Grid item>
              <Box
                display="flex"
                alignItems="center"
                justifyContent="space-between"
              >
                <CustomButton
                  className={classes.btn}
                  type="button"
                  onClick={() =>
                    setField({ type: "BACK_STEPP", payload: null })
                  }
                  right
                >
                  بازگشت
                </CustomButton>
                <CustomButton className={classes.btn} left type="submit">
                  مرحله بعد
                </CustomButton>
              </Box>
            </Grid>
          </Grid>
        </form>
      </FormProvider>
    </>
  );
}
