import React from "react";
import { AppBar, Box, Hidden, makeStyles, Typography } from "@material-ui/core";
import PersonIcon from "@material-ui/icons/Person";
import logo from "../../assets/image/logo.svg";

const useStyles = makeStyles((theme) => ({
  root: {
    background: "none",
    boxShadow: "none",
    height: "80px",
  },
  mainBox: {
    padding: "2rem 3rem",
  },
  title: {
    fontFamily: "Vazir-Bold",
    fontWeight: "bold",
  },
  subject: {
    fontSize: "1.1rem !important",
  },
}));

export default function Navbar({ fName, lName }) {
  const classes = useStyles();
  return (
    <AppBar position="fixed" className={classes.root}>
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        className={classes.mainBox}
      >
        <Box className={classes.logBox}>
          <img src={logo} alt="logo" />
        </Box>
        <Hidden smDown>
          <Typography className={classes.title}>
            سامانه مقایسه و خرید انلاین بیمه
          </Typography>
        </Hidden>
        <Box display="flex" alignItems="center">
          {fName && <PersonIcon />}
          <Typography className={classes.subject}>
            {fName ? `${fName} ${lName}` : "ثبت نام"}
          </Typography>
        </Box>
      </Box>
    </AppBar>
  );
}
