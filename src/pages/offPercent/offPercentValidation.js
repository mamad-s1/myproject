import * as Yup from "yup";

const schema = Yup.object().shape({
  thirdOff: Yup.number().required("لطفا درصد تخفیف ثالث را وارد کنید"),
  riderOff: Yup.number().required("لطفا درصد تخفیف حوادث راننده را وارد کنید"),
});

export default schema;
