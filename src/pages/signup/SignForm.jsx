import React from "react";
import { Grid, makeStyles, Typography } from "@material-ui/core";
import { useForm, FormProvider } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import CustomInput from "../../components/customInput/CustomInput";
import CustomButton from "../../components/customButton/CustomButton";
import schema from "./signupValidation";

const useStyles = makeStyles((theme) => ({
  title: {
    fontFamily: "Vazir-Bold",
    marginBottom: "2rem",
    fontSize: "1.5rem",
  },
}));

export default function Signup({ setField }) {
  const classes = useStyles();
  const methods = useForm({
    resolver: yupResolver(schema),
  });
  const { handleSubmit } = methods;
  const onSubmitSignup = (data) => {
    setField({ type: "SET_USER_PROFILE", payload: data });
    setField({ type: "NEXT_STEPP", payload: null });
  };
  return (
    <>
      <Typography className={classes.title} variant="h1">
        ثبت نام
      </Typography>
      <FormProvider {...methods}>
        <form onSubmit={handleSubmit(onSubmitSignup)}>
          <Grid container item direction="column" spacing={2} md={9}>
            <Grid item container spacing={2}>
              <Grid item md={6} xs={12}>
                <CustomInput name="firstName" label="نام" type="text" />
              </Grid>
              <Grid item md={6} xs={12}>
                <CustomInput name="lastName" label="نام خانوادگی" type="text" />
              </Grid>
            </Grid>
            <Grid item>
              <CustomInput name="mobile" label="شماره موبایل" type="number" />
            </Grid>
            <Grid item>
              <CustomInput name="password" label="رمز عبور" type="password" />
            </Grid>
            <Grid item dir="ltr">
              <CustomButton type="submit">ثبت نام</CustomButton>
            </Grid>
          </Grid>
        </form>
      </FormProvider>
    </>
  );
}
