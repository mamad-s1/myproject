import * as Yup from "yup";

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
const persianRegExp = /^[\u0600-\u06FF\uFB8A\u067E\u0686\u06AF\u200C\u200F]+$/;
const passwordLengthRegExo = /^.{4,10}$/;
const passwordCharRegExp = /^[\x20-\x7E]+$/;
const uppercaseRegExp = /[A-Z]/;
const lowercaseRegExp = /[a-z]/;

const schema = Yup.object().shape({
  firstName: Yup.string()
    .matches(persianRegExp, "لطفا زبان کیبورد خود را به فارسی تغییر دهید")
    .required("لطفا نام خود را وارد کنید"),
  lastName: Yup.string()
    .required("لطفا نام‌حانوادگی خود را وارد کنید")
    .matches(persianRegExp, "لطفا زبان کیبورد خود را به فارسی تغییر دهید"),

  mobile: Yup.string()
    .matches(phoneRegExp, "لطفا شماره همراه خود را به صورت صحیح وارد کنید")
    .min(11, "لطفا شماره همراه خود را به صورت صحیح وارد کنید")
    .max(11, "لطفا شماره همراه خود را به صورت صحیح وارد کنید"),
  password: Yup.string()
    .matches(passwordLengthRegExo, "باید شامل حداقل 4 و حداکثر 10 کراکتر بشود")
    .matches(passwordCharRegExp, "از اعداد و حروف انگلیسی استفاده کنید")
    .matches(uppercaseRegExp, "باید شامل حداقل یک حرف بزرگ باشد")
    .matches(lowercaseRegExp, "باید شامل حداقل یک حرف کوچیک باشد")
    .required("لطفا پسورد خود را وارد کنید"),
});

export default schema;
