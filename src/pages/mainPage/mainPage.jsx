import React, { useReducer } from "react";
import { Grid, makeStyles } from "@material-ui/core";
import IconSteper from "./components/stepers/IconSteper";
import DotsSteper from "./components/stepers/DotsSteper";
import Step1 from "./components/step1/Step1";
import Step2 from "./components/step2/Step2";
import Step3 from "./components/step3/Step3";
import Step4 from "./components/step4/Step4";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
}));

const STEPS = [
  "اطلاعات عمومی",
  "داستان پروژه",
  "اطلاعات ایجاد کننده",
  "تایید نهایی",
];

function reducer(stepsState, action) {
  switch (action.type) {
    case "NEXT_STEPP":
      return { ...stepsState, activeStep: stepsState.activeStep + 1 };
    case "SET_STEPP":
      return {
        ...stepsState,
        stepsValue: { ...stepsState.stepsValue, ...action.payload },
      };
    default:
      break;
  }
  return stepsState;
}

const initialState = {
  activeStep: 0,
  stepsValue: {},
};

export default function CreateCrowdfundingProjectDesktop() {
  const classes = useStyles();
  const [stepsState, dispatch] = useReducer(reducer, initialState);
  const { activeStep, stepsValue } = stepsState;

  console.log("steeeep: ", stepsState);

  const nextStepHandler = () => {
    dispatch({
      type: "NEXT_STEPP",
    });
  };

  const setStepDataHandler = (value) => {
    dispatch({
      type: "SET_STEPP",
      payload: { ...value },
    });
  };

  const getStep = (key) => {
    switch (key) {
      case 0:
        return (
          <Step1
            setStep={nextStepHandler}
            setStepData={(value) => setStepDataHandler(value)}
          />
        );
      case 1:
        return (
          <Step2
            setStep={nextStepHandler}
            setStepData={(value) => setStepDataHandler(value)}
          />
        );
      case 2:
        return (
          <Step3
            setStep={nextStepHandler}
            setStepData={(value) => setStepDataHandler(value)}
          />
        );
      case 3:
        return <Step4 setpsData={stepsValue} />;
      default:
        return null;
    }
  };

  return (
    <div className={classes.root}>
      <Grid container direction="column">
        <Grid item style={{ backgroundColor: "#fff", position: "relative" }}>
          <IconSteper steps={STEPS} activeStep={activeStep} />
          <DotsSteper steps={STEPS} activeStep={activeStep} />
        </Grid>
        <Grid item style={{ backgroundColor: "#f8f9fe" }} xs={12}>
          {getStep(activeStep)}
        </Grid>
      </Grid>
    </div>
  );
}
