import axios from "axios";
import { ServerErrorHandling } from "./ServerErrorHandling";
const cors = require("cors");

const config = {
  //   cors_api_host: "cors-anywhere.herokuapp.com",
  baseURL: "https:///bimito.com/bimito/core/data/",
  timeout: 50000,
  responseType: "json",
  xsrfCookieName: "XSRF-TOKEN",
  xsrfHeaderName: "X-XSRF-TOKEN",
  onUploadProgress: (progressEvent) => {},
  onDownloadProgress: (progressEvent) => {},
  maxContentLength: 2000,
  maxBodyLength: 2000,
  validateStatus: (status) => status === 200 && status < 300,
  maxRedirects: 5,
  cancelToken: new axios.CancelToken((cancel) => {}),
};

const ApiCreator = async (IapiCreator) => {
  const customConfig = {
    ...config,
    headers: {
      "Access-Control-Allow-Origin": "*",
      token: "faketoken",
      language: "en-US",
      ...(IapiCreator.headers && { ...IapiCreator.headers }),
    },
    url: IapiCreator.url,
    method: IapiCreator.method,
    data: IapiCreator.data,
  };

  return await axios(customConfig)
    .then((response) => response.data.data)
    .catch((err) => ServerErrorHandling(err, IapiCreator.toast));
};

class Api {
  static GET = (Iapi) =>
    ApiCreator({
      headers: Iapi.header,
      method: "get",
      url: Iapi.url,
      data: Iapi.data,
      token: Iapi.token,
      toast: Iapi.toast,
    });

  static POST = (Iapi) =>
    ApiCreator({
      headers: Iapi.header,
      method: "post",
      url: Iapi.url,
      data: Iapi.data,
      token: Iapi.token,
      toast: Iapi.toast,
    });

  static PUT = (Iapi) =>
    ApiCreator({
      headers: Iapi.header,
      method: "put",
      url: Iapi.url,
      data: Iapi.data,
      token: Iapi.token,
      toast: Iapi.toast,
    });

  static PATCH = (Iapi) =>
    ApiCreator({
      headers: Iapi.header,
      method: "patch",
      url: Iapi.url,
      data: Iapi.data,
      token: Iapi.token,
      toast: Iapi.toast,
    });

  static DELETE = (Iapi) =>
    ApiCreator({
      headers: Iapi.header,
      method: "delete",
      url: Iapi.url,
      data: Iapi.data,
      token: Iapi.token,
      toast: Iapi.toast,
    });
}

export default Api;
