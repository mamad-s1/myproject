import {
  unstable_createMuiStrictModeTheme as createMuiTheme,
  responsiveFontSizes,
} from "@material-ui/core";

const defaultTheme = createMuiTheme();

const mamadTheme = createMuiTheme({
  direction: "rtl",
  typography: {
    fontFamily: [
      "VazirFD",
      "Vazir",
      "Roboto",
      "Helvetica",
      "Arial",
      "sans-serif",
    ].join(","),
    h1: {
      fontSize: "3.636rem",
      fontWeight: "normal",
      lineHeight: 1.58,
    },
    h2: {
      fontSize: "2.909rem",
      fontWeight: "normal",
      lineHeight: 1.56,
    },
    h3: {
      fontSize: "2.545rem",
      fontWeight: "normal",
      lineHeight: 1.54,
    },
    h4: {
      fontSize: "2.181rem",
      fontWeight: "normal",
      lineHeight: 1.54,
    },
    h5: {
      fontSize: "1.818rem",
      fontWeight: "normal",
      lineHeight: 1.55,
    },
    h6: {
      fontSize: "1.454rem",
      fontWeight: "normal",
      lineHeight: 1.56,
      [defaultTheme.breakpoints.down("md")]: {
        fontSize: "1rem !important",
      },
    },
    body1: {
      fontSize: "1.272rem",
      fontWeight: "normal",
      lineHeight: "1.57",
      color: "#000000",
      [defaultTheme.breakpoints.only("lg")]: {
        fontSize: "1.1739rem !important",
      },
      [defaultTheme.breakpoints.down("md")]: {
        fontSize: "1.09rem !important",
      },
    },
    caption: {
      color: "#8898aa",
      fontWeight: "300",
      fontSize: "1.272rem",
      lineHeight: "2",
      [defaultTheme.breakpoints.down("md")]: {
        fontSize: "1.181rem !important",
      },
    },
  },
  palette: {
    primary: {
      main: "#d4d4d4",
    },
    secondary: {
      main: "#25b79b",
    },
    success: {
      main: "#30c0b0",
    },
    error: {
      main: "#f5365c",
    },
    warning: {
      main: "#ffbb01",
    },
  },
  overrides: {
    MuiFormLabel: {
      root: {
        [defaultTheme.breakpoints.up("xs")]: {
          fontSize: "1rem !important",
        },
      },
    },
    MuiInputLabel: {
      root: {
        color: "#d4d4d4",
      },
      outlined: {
        transform: "translate(14px, 14px) scale(1)",
      },
    },
    MuiOutlinedInput: {
      root: {
        height: "45px",
      },
    },
    MuiFormHelperText: {
      root: {
        [defaultTheme.breakpoints.up("xs")]: {
          fontSize: ".7rem !important",
        },
      },
    },
    MuiButton: {
      root: {
        height: "40px !important",
        textTransform: "initial",
        width: "120px",
        padding: "9px 0",
        "&:hover": {
          transform: "translateY(-1px)",
          transition: "all .15s ease",
          backgroundColor: "none",
        },
      },
      label: {
        display: "flex",
        alignItems: "center",
      },
    },
  },
});

export default responsiveFontSizes(mamadTheme);
