import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { ThemeProvider } from "@material-ui/core/styles";
import RTL from "./assets/theme/rtl";
import myTheme from "./assets/theme/theme";
import "./assets/styles/global.css";

ReactDOM.render(
  <React.StrictMode>
    <RTL>
      <ThemeProvider theme={myTheme}>
        <App />
      </ThemeProvider>
    </RTL>
  </React.StrictMode>,
  document.getElementById("root")
);
