import React from "react";
import { useFormContext, Controller } from "react-hook-form";
import { TextField } from "@material-ui/core";

function FormInput(props) {
  const { control } = useFormContext();
  const { name, label, type } = props;

  return (
    <Controller
      render={({
        field: { onChange, onBlur, value, ref },
        fieldState: { invalid, isTouched, isDirty, error },
      }) => {
        return (
          <TextField
            onChange={onChange}
            onBlur={onBlur}
            inputRef={ref}
            label={label}
            fullWidth={true}
            error={!!error}
            helperText={error?.message}
            type={type}
            variant="outlined"
          />
        );
      }}
      name={name}
      control={control}
    />
  );
}

export default FormInput;
