import React from "react";
import clsx from "clsx";
import { Button, makeStyles } from "@material-ui/core";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";

const useStyles = makeStyles((theme) => ({
  root: {
    background: [theme.palette.secondary.main],
    color: "#fff",
    boxShadow: "none",
    borderRadius: "2rem",
    display: "flex",
    alignItems: "center",
  },
}));

export default function CustomButton(props) {
  const classes = useStyles();
  const { children, type, className, right, left, ...other } = props;

  return (
    <>
      <Button type={type} className={clsx(classes.root, className)} {...other}>
        {right && <ChevronRightIcon />}
        {children}
        {left && <ChevronLeftIcon />}
      </Button>
    </>
  );
}
