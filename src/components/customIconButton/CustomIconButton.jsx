import React from "react";
import clsx from "clsx";
import { withStyles } from "@material-ui/core/styles";
import { Box, IconButton, Typography } from "@material-ui/core";

const styles = {
  root: {
    width: "120px",
    border: "1px solid #000",
    borderRadius: "1rem",
    padding: "1.5rem .5rem",
    "&$disabled": {
      background: "rgba(0, 0, 0, 0.12)",
      opacity: ".3",
    },
  },
  defLable: {
    fontSize: ".8rem !important",
    fontFamily: "Vazir-Bold",
  },
  iconBox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
  },
  disabled: {},
};

function CustomIconButton(props) {
  const { classes, lable, className, icon, size, ...other } = props;

  return (
    <IconButton
      className={clsx(classes.root, className)}
      classes={{ root: classes.root, disabled: classes.disabled }}
      aria-label="aria"
      {...other}
    >
      <Box className={classes.iconBox}>
        <Box style={{ width: `${size}` }}>
          <img src={icon} alt="insuranceImg" />
        </Box>
        <Typography className={classes.defLable}>{lable}</Typography>
      </Box>
    </IconButton>
  );
}

export default withStyles(styles)(CustomIconButton);
