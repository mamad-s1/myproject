import { Typography } from "@material-ui/core";
import React from "react";
import { useFormContext, Controller } from "react-hook-form";
import ReactSelect from "react-select";

function CustomSelect(props) {
  const { control } = useFormContext();
  const { name, label, options, placeholder, ...other } = props;

  return (
    <>
      <Typography>{label}</Typography>
      <Controller
        render={({
          field: { onChange, value, label, ref },
          fieldState: { invalid, isTouched, isDirty, error },
        }) => {
          return (
            <>
              <ReactSelect
                inputRef={ref}
                options={options}
                onChange={(val) => onChange(val?.value)}
                isClearable
                placeholder={placeholder}
              />
              {error && (
                <Typography variant="body2" style={{ color: "red" }}>
                  {error.message}
                </Typography>
              )}
            </>
          );
        }}
        control={control}
        name={name}
        {...other}
      />
    </>
  );
}

export default CustomSelect;
